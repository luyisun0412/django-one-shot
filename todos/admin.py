from django.contrib import admin
from todos.models import TodoList, TodoItem
# Register your models here.

# register model and display model in the database
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name"
    ]

@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed"
    ]
